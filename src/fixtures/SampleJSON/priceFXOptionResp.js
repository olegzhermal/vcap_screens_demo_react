export default {
   "fxoption_pricing_resp":{
      "input":{
         "counterpart":"COMPANY A",
         "instrument":"EUR/USD",
         "b/s":"Buy",
         "payout":"Call EUR/Put USD",
         "expiry":"1M",
         "strike":"1.1350",
         "nominal":"1000000",
         "nominal currency":"EUR",
         "margin":"0.01",
         "margin mode":"%EUR",
         "margin amount":"1000.00",
         "margin amount currency":"EUR",
         "premium":"",
         "premium currency":"",
      },
      "output":{
         "spot":"1.1200",
         "client volatilty":"7.85",
         "strike":"1.1350",
         "delta":"40.83",
         "premium":"3475",
         "premium currency":"EUR",
         "margin amount":"1002.83",
         "margin amount currency":"EUR"
      }
   }
}
