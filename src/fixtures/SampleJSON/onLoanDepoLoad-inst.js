export default {
   "getLoanInstruments":{
      "userID":"300100",
      "counterpartyID":"70001984"
   },
   "fixed":{
      "instrument":[
         "EUR",
         "USD",
         "JPY",
         "GBP",
         "SGD",
         "THB",
         "MYR",
         "INR",
         "HKD",
         "AUD",
         "CNY"
      ]
   },
   "floating":[
      {
         "instrument":"USD LIBOR 3M",
         "currency":"USD"
      },
      {
         "instrument":"USD LIBOR 1M",
         "currency":"USD"
      },
      {
         "instrument":"SIBOR 3M",
         "currency":"SGD"
      },
      {
         "instrument":"SIBOR 6M",
         "currency":"SGD"
      }
   ]
}
