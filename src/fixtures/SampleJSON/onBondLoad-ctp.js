export default {
   "getCounterparty":{
      "userID":"300100",
      "asset":"FI",
      "product":"Bond"
   },
   "counterpart":[
      {
         "label":"COMPANY A",
         "id":"70001984",
         "tier":"Gold"
      },
      {
         "label":"COMPANY B",
         "id":"70007931",
         "tier":"Silver"
      },
      {
         "label":"COMPANY C",
         "id":"70005610",
         "tier":"Bronze"
      }
   ]
}
