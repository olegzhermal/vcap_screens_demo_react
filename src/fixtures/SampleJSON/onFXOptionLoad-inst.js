export default {
  "getCurrencyPairs": {
    "userID":"300100",
    "counterpartyID":"70001984"
  },
   "instrument":[
      "EUR/USD",
      "USD/JPY",
      "GBP/USD",
      "USD/CHF",
      "USD/SGD",
      "USD/THB",
      "USD/MYR",
      "USD/INR",
      "USD/HKD",
      "AUD/USD",
      "USD/CNY",
      "AUD/JPY",
      "EUR/JPY"
   ]
}
