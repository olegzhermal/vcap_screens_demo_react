export default {
   "getCounterparty":{
      "userID":"300100",
      "asset":"MM",
      "product":"Loan"
   },
   "counterpart":[
      {
         "label":"COMPANY A",
         "id":"70001984",
         "tier":"Gold"
      },
      {
         "label":"COMPANY B",
         "id":"70007931",
         "tier":"Silver"
      }
   ]
}
