export default {
   "loandepo_pricing_resp":{
      "input":{
         "counterpart":"COMPANY A",
         "nature":"fixed",
         "instrument":"USD",
         "payout":"Borrow",
         "start":"20/02/2017",
         "maturity":"20/08/2017",
         "amortizing":"None",
         "nominal":"1000000",
         "nominal currency":"USD",
         "margin":"0.01",
         "net rate":""
      },
      "output":{
         "rate":"0.3920114",
         "net rate":"0.40201114",
         "repayment":"1004020",
         "repayment currency":"USD",
         "margin":"3475",
         "margin currency":"EUR"
      }
   }
}
