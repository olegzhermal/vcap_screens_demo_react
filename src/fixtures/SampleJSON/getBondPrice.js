export default {
   "getBondPrice":{
      "input":{
         "ISIN":"ABN0910934001",
         "Instrument Label":"ABN 3% A CPN 103"
      },
      "output":{
         "issuer":"US Treasury",
         "sec code":"US09128JO09093",
         "coupon":"3%",
         "maturity":"01/01/2020",
         "country":"US",
         "currency":"USD",
         "collateral":"Snr secured",
         "S&P":"AA+",
         "Moodys":"AA+",
         "Px to client":"100.30"
      }
   }
}
