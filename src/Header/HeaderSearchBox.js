import React from 'react';
import { FormControl } from 'react-bootstrap';

export default ({ className }) => (
  <div className={className}>
    <div>
      <i className="fa fa-search header-item" aria-hidden="true" />
    </div>
    <FormControl type="text" />
  </div>
)
