import React from 'react';
import classnames from 'classnames';
import HeaderSearchBox from './HeaderSearchBox';
import './index.css';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import profileImage from '../assets/profile/pics/sdgHGj78aLKJH98.png';

const ProfilePhoto = () => <img className="dropdown-profile-photo" src={profileImage} alt="profile"/>

const getHeaderContent = ({
  section,
  navigationContainerClassName,
  headerSearchClassName,
}) => {
  switch (section) {
    case 'dashboard':
      return <HeaderSearchBox className={headerSearchClassName}/>;
    default: return <HeaderSearchBox className={headerSearchClassName}/>;
  }
}

const Header = ({
  changeTheme,
  navigationHidden,
  section,
  ordersBodySection,
  changeOrdersBody,
  theme,
}) => {
  const styles = {
    headerSearch: classnames(
      'header-search',
      { 'header-search-navigation-hidden': navigationHidden },
    ),
    navigationContainerClassName: classnames(
      'header-content',
      { 'header-content-navigation-hidden': navigationHidden },
    ),
  }

  const headerContent = getHeaderContent({
    section,
    headerSearchClassName: styles.headerSearch,
    navigationContainerClassName: styles.navigationContainerClassName,
    changeOrdersBody,
    ordersBodySection,
  })

  return (
    <header className={classnames('header-container', `header-container-${theme}`)}>
      {headerContent}
      <div className='header-container-pull-right'>
        <i className="fa fa-envelope-o header-item" aria-hidden="true"></i>
        <span>Oleg Zhermal</span>
        <DropdownButton
          id="dd-0"
          className="header-item header-profile-dropdown"
          bsStyle="default"
          title={<ProfilePhoto />}
        >
          <MenuItem eventKey="1" onClick={changeTheme('light')}>Light Theme</MenuItem>
          <MenuItem eventKey="2" onClick={changeTheme('dark')}>Dark Theme</MenuItem>
          <MenuItem eventKey="3">Option 3</MenuItem>
        </DropdownButton>
      </div>
    </header>
  )
}

export default Header;
