import React from 'react';
import './Accounts.css';
import AccountsBox from './AccountsBox';
import AccountCard from './AccountCard';
import News from './News';
import TradeBlotter from './TradeBlotter';
import diag0 from '../assets/images/photo_1.jpg';
import diag1 from '../assets/images/photo_2.jpg';
import diag2 from '../assets/images/photo_3.jpg';

const Accounts = (props) => (
  <div className="accounts">
    <div className="accounts-head">
      <i className="fa fa-bars" aria-hidden="true"></i>
      <span className="dashboard">Dashboard</span>
    </div>
    <div className="accounts-body">
      <AccountsBox header="Account Card">
        <AccountCard/>
      </AccountsBox>
      <AccountsBox header="Portfolio Perfomans">
        <img src={diag0} alt="diagram"/>
      </AccountsBox>
      <AccountsBox header="Trade Alocation">
        <img src={diag1} alt="diagram"/>
      </AccountsBox>
      <AccountsBox header="Trade Blotter">
        <TradeBlotter />
      </AccountsBox>
      <AccountsBox header="Interractions Timeline">
        <img src={diag2} alt="diagram"/>
      </AccountsBox>
      <AccountsBox header="News" children={<News/>}/>
    </div>
  </div>
)

export default Accounts;
