import React from 'react';

export default function AccountsBox(props) {

  const {header, children} = props;

    return (
      <div className="accounts-box">
        <div className="account-box-head">
          {header}
        </div>
        <div className="account-box-body">
          <div className="dots"><i className="fa fa-ellipsis-v" aria-hidden="true"></i></div>
          <div>
            {children}
          </div>
        </div>
      </div>
    )
}
