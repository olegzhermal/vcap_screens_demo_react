import React from 'react';

export default function News(props) {

    return (
      <div className="news">
        <div className="news-item">
          <div className="icon-news">
            <i className="fa fa-user" aria-hidden="true"></i>
          </div>
          <div className="news-body">
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam quam nesciunt doloremque.</div>
            <div>Lorem ipsum.</div>
            <div>Lorem ipsum.</div>
            <div className="news-item-last">
              <div>{"PLACED AN ORDER"}</div>
              <div></div>
            </div>
          </div>
        </div>
        <div className="news-item">
          <div className="icon-news">
            <i className="fa fa-user" aria-hidden="true"></i>
          </div>
          <div className="news-body">
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam quam nesciunt doloremque.</div>
            <div>Lorem ipsum.</div>
            <div>Lorem ipsum.</div>
            <div className="news-item-last2">
              <div>{"AUCTION DROP"}</div>
              <div></div>
            </div>
          </div>
        </div>
        <div className="news-item">
          <div className="icon-news">
            <i className="fa fa-user" aria-hidden="true"></i>
          </div>
          <div className="news-body">
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam quam nesciunt doloremque.</div>
            <div>Lorem ipsum.</div>
            <div>Lorem ipsum.</div>
            <div className="news-item-last2">
              <div>{"AUCTION DROP"}</div>
              <div></div>
            </div>
          </div>
        </div>
      </div>
    )
  }
