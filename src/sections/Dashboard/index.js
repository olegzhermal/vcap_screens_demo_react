import React, { Component } from 'react';
import './index.css';
import BodyBox from '../../common/BodyBox/index.js';
import NotificationBox from '../../common/NotificationBox/index.js';

class Dashboard extends Component {
  state = {
    notification: true,
  }

  hideNotification = () => {
    this.setState({ notification: false });
  }

  render() {
    return (
      <div className={`dashboard-${this.props.theme}`}>
        {
          this.state.notification &&
          <NotificationBox onClick={this.hideNotification}>
            Click here to hide this notification box (logic for appearing and hiding should be implemented)
          </NotificationBox>
        }
        <div className="dashboard-masonry">
          <BodyBox header="Header">
            <div>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque quod officia consectetur, deserunt, iusto nobis fugiat placeat eum voluptatum, ratione quas libero, eius alias quasi error asperiores aspernatur. Voluptate nesciunt tenetur, facilis, ad maiores animi ducimus recusandae facere enim. Cum corrupti magnam iusto officiis provident atque aliquam ullam neque repellendus, aut nihil quae fuga fugiat expedita, earum dolorum odio, cumque quaerat! Consectetur dolorum, illum voluptatum cumque laboriosam quas, soluta earum, ipsam facilis quae ea esse id molestiae ipsum commodi quaerat! Optio animi facilis, voluptatum nulla molestiae illum eius ad natus consectetur incidunt quis unde, similique labore ipsa, nisi magni deserunt?
            </div>
          </BodyBox>
          <BodyBox header="Header">
            <div>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus voluptate provident et veritatis nostrum, inventore repudiandae odio est alias dolor temporibus.
            </div>
          </BodyBox>
          <BodyBox header="Header">
            <div>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus voluptate provident et veritatis nostrum, inventore repudiandae odio est alias dolor temporibus. Libero facere accusantium voluptate cupiditate aspernatur magnam reprehenderit sequi.
            </div>
          </BodyBox>
          <BodyBox header="Header">
            <div>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus voluptate provident et veritatis nostrum,
            </div>
          </BodyBox>
          <BodyBox header="Header">
            <div>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus voluptate provident et veritatis nostrum, inventore repudiandae odio est alias dolor temporibus. Libero facere accusantium voluptate cupiditate aspernatur magnam reprehenderit sequi.
            </div>
          </BodyBox>
          <BodyBox header="Header">
            <div>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus voluptate provident et veritatis nostrum, inventore repudiandae odio est alias dolor temporibus. Libero facere accusantium voluptate cupiditate aspernatur magnam reprehenderit sequi.
            </div>
          </BodyBox>
        </div>
      </div>
    )
  }
}

export default Dashboard
