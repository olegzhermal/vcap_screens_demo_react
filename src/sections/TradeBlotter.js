import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

const data = [
  {
    tradeNb: 'Lorem 1',
    tradeAs: 'Ipsum 1',
    shotdown: 'Dolor 1',
    tradeDate: '13.02.2017',
    quantity: 534,
  },
  {
    tradeNb: 'Lorem 2',
    tradeAs: 'Ipsum 2',
    shotdown: 'Dolor 2',
    tradeDate: '23.02.2017',
    quantity: 456,
  },
  {
    tradeNb: 'Lorem 3',
    tradeAs: 'Ipsum 3',
    shotdown: 'Dolor 3',
    tradeDate: '13.05.2017',
    quantity: 234,
  },
 ];

 const columns = [
  {
    Header: 'Trade NB',
    accessor: 'tradeNb',
  },
  {
    Header: 'Trade AS',
    accessor: 'tradeAs',
  },
  {
    Header: 'Shutdown',
    accessor: 'shotdown',
  },
  {
    Header: 'Trade Date',
    accessor: 'tradeDate',
  },
  {
    Header: 'Quantity',
    accessor: 'quantity',
  },
];

const TradeBlotter = (props) => (
  <ReactTable
    data={data}
    columns={columns}
    defaultPageSize={5}
  />
)

export default TradeBlotter;
