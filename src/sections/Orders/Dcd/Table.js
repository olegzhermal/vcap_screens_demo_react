import React from 'react';

// const demoData = {
//   columns: [
//     {
//       text: 'EUR/USD',
//       accessor: 'eurUsd',
//     },
//     {
//       text: 'EUR/GPB',
//       accessor: 'eurGpb',
//     },
//     {
//       text: 'EUR/HKD',
//       accessor: 'eurHkd',
//     },
//     {
//       text: 'EUR/AUD',
//       accessor: 'eurAud',
//     },
//     {
//       text: 'EUR/SGD',
//       accessor: 'eurSgd',
//     },
//   ],
//   rows: [
//     'EUR',
//     'CURRENCY SPOT',
//     'ATM',
//     '15% DELTA',
//     '25% DELTA',
//   ],
//   data: [
//     {}
//   ],
// }

export default ({data, theme}) => (
  <table className={`dcd-table dcd-table-${theme}`}>
    <thead>
      <tr>
        <th></th>
        <th>EUR/USD</th>
        <th>EUR/GPB</th>
        <th>EUR/HKD</th>
        <th>EUR/AUD</th>
        <th>EUR/SGD</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>EUR</td>
        <td>USD</td>
        <td>GPB</td>
        <td>HKD</td>
        <td>AUD</td>
        <td>SGD</td>
      </tr>
      <tr>
        <td>CURRENCY SPOT</td>
        <td className="dcd-table-data-grey">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data-grey">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data-grey">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data-grey">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data-grey">{Math.random().toFixed(4)}</td>
      </tr>
      <tr>
        <td>ATM</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
      </tr>
      <tr>
        <td>15% DELTA</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
      </tr>
      <tr>
        <td>25% DELTA</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
        <td className="dcd-table-data">{Math.random().toFixed(4)}</td>
      </tr>
    </tbody>
  </table>
)
