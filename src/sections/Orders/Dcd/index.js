import React, { Component } from 'react';
import Table from './Table';
import { DropdownButton, MenuItem, Form, FormControl } from 'react-bootstrap';
import Button from '../../../common/FormItems/Button';
import CustomDT from '../../../common/FormItems/CustomDT.js';
import TB3Input from '../../../common/FormItems/TB3Input.js';
import TGInput from '../../../common/FormItems/TGInput.js';
import './index.css';

class Dcd extends Component {
  render() {
    return (
      <div className={`orders-section-container dcd-container-${this.props.theme}`}>
        <div className="dcd-row">
          <div className="dcd-data">
            <div className="dcd-item-part">
              Counterparty
            </div>
            <div className="dcd-item-part">
              <Form className="blue-input">
                <FormControl type="text" defaultValue={(Math.random()*10).toFixed(4)} />
              </Form>
            </div>
            <div className="dcd-item-part color-gold">
              Gold
            </div>
          </div>
          <div className="dcd-data">
            <div className="dcd-item-part">
              Maturity
            </div>
            <div className="dcd-item-part">
              <CustomDT theme={this.props.theme}/>
            </div>
          </div>
        </div>


        <div className="dcd-row">
          <div className="dcd-data">
            <div className="dcd-item-part">
              DEPO CCY
            </div>
            <div className="dcd-item-part">
              <DropdownButton id="dd-5" className={`loan-depo-dropdown-${this.props.theme} loan-depo-inputs dropdown-button`} bsStyle="default" title="EUR">
                <MenuItem eventKey="1">USD</MenuItem>
                <MenuItem eventKey="2">Option 2</MenuItem>
                <MenuItem eventKey="3">Option 3</MenuItem>
              </DropdownButton>
            </div>
          </div>
          <div className="dcd-data">
            <div className="dcd-item-part">
              Nominal
            </div>
            <div className="dcd-item-part">
              <TB3Input style={{color: '#1789F5'}} />
            </div>
            <div className="dcd-item-part">
              <TGInput
                className="loan-depo-dropdown loan-depo-inputs dropdown-button"
                onText="EUR"
                offText="USD"
                hwidth={90}
              />
            </div>
          </div>
          </div>

        <div className="dcd-buttons-group-bottom">
          <Button color="blue" theme={this.props.theme}>Price</Button>
        </div>

        <hr/>
        <div className={`dcd-table-container-${this.props.theme}`}>
          <Table theme={this.props.theme} />
        </div>

    </div>
    );
  }
}

export default Dcd;
