import React from 'react';
import { Form, FormControl } from 'react-bootstrap';
import Button from '../../../common/FormItems/Button';
import TB2Input from '../../../common/FormItems/TB2Input.js';
import TB3Input from '../../../common/FormItems/TB3Input.js';
import RSInput from '../../../common/FormItems/RSInput.js';
import onBondLoad from '../../../fixtures/SampleJSON/onBondLoad-inst.js';
import onBondLoadCtp from '../../../fixtures/SampleJSON/onBondLoad-ctp.js';
import './index.css';
import bondPrice from '../../../fixtures/SampleJSON/getBondPrice.js';

const rsInputOptions = onBondLoadCtp.counterpart.map(({ label }) => (
  {
    value: label,
    label
  }
))
const instrumentOptions = onBondLoad.instrument.map(o => (
  {
    value: o['Instrument Label'],
    label: o['Instrument Label'],
  }
));

export default ({theme}) => (
  <div className="orders-section-container">
    {/*(div[className="bonds-row"]>(div[className="bonds-data"]>(div[className="bonds-item-part"]>lorem2)*2)*2)*2*/}
    <div className="bonds-row">
      <div className="bonds-data-trisection">
        <div className="bonds-item-part">
          Counterparty
        </div>
        <div className="bonds-item-part">
          <RSInput value="" options={rsInputOptions}/>
        </div>
        <div className="bonds-item-part color-gold">
          Gold
        </div>
      </div>

      <div className="bonds-data">
        <div className="bonds-item-part">
          Issuer
        </div>
        <div className="bonds-item-part">
          <Form>
            <FormControl className="bonds-text-input" type="text" value={bondPrice.getBondPrice.output['issuer']} onChange={()=>{}} />
          </Form>
        </div>
      </div>
    </div>

    <div className="bonds-row">
      <div className="bonds-data-trisection">
        <div className="bonds-item-part">
          Instrument
        </div>
        <div className="bonds-item-part">
          <RSInput value="" options={instrumentOptions}/>
        </div>
        <div className="bonds-item-part color-gold">
        </div>
      </div>
      <div className="bonds-data">
        <div className="bonds-item-part">
          Sec code
        </div>
        <div className="bonds-item-part">
          <Form>
            <FormControl className="bonds-text-input" type="text" value={bondPrice.getBondPrice.output['sec code']} onChange={()=>{}}/>
          </Form>
        </div>
      </div>
    </div>

    <div className="bonds-row">
      <div className="bonds-data-trisection">
        <div className="bonds-item-part">
          Price to Client
        </div>
        <div className="bonds-item-part">
          <TB2Input style={{color: "#A9B1B6"}} value={bondPrice.getBondPrice.output['Px to client']} />
        </div>
        <div className="bonds-item-part">
          %
        </div>
      </div>
      <div className="bonds-data">
        <div className="bonds-item-part">
          Coupon
        </div>
        <div className="bonds-item-part">
          <Form>
            <FormControl className="bonds-text-input" type="text" value={bondPrice.getBondPrice.output['coupon']} onChange={()=>{}} />
          </Form>
        </div>
      </div>
    </div>

    <div className="bonds-row">
      <div className="bonds-data-trisection">
        <div className="bonds-item-part">
          Client Buys
        </div>
        <div className="bonds-item-part">
          <TB3Input style={{color: '#1789F5'}} />
        </div>
        <div className="bonds-item-part">
          USD
        </div>
      </div>
      <div className="bonds-data">
        <div className="bonds-item-part">
          Maturity
        </div>
        <div className="bonds-item-part">
          <Form>
            <FormControl className="bonds-text-input" type="text" value={bondPrice.getBondPrice.output['maturity']} onChange={()=>{}} />
          </Form>
        </div>
      </div>
    </div>

    <div className="bonds-row">
      <div className="bonds-data-trisection">
        <div className="bonds-item-part">
          Client Pays
        </div>
        <div className="bonds-item-part">
          <TB3Input style={{color: '#1789F5'}} />
        </div>
        <div className="bonds-item-part color-gold" />
      </div>
      <div className="bonds-data">
        <div className="bonds-item-part">
          Country
        </div>
        <div className="bonds-item-part-non-standard">
          <div>
            <Form>
              <FormControl className="bonds-text-input" type="text" style={{color: "#A9B1B6"}} value={bondPrice.getBondPrice.output['country']} onChange={()=>{}} />
            </Form>
          </div>
          <div>
            Cur
          </div>
          <div>
            <Form>
              <FormControl className="bonds-text-input" type="text" style={{color: "#A9B1B6"}} value={bondPrice.getBondPrice.output['currency']} onChange={()=>{}} />
            </Form>
          </div>
          </div>
        </div>
    </div>

    <div className="bonds-row-right">
      <div className="bonds-data">
        <div className="bonds-item-part">
          Collateral
        </div>
        <div className="bonds-item-part">
          <Form>
            <FormControl className="bonds-text-input" type="text" value={bondPrice.getBondPrice.output['collateral']} onChange={()=>{}} />
          </Form>
        </div>
      </div>
    </div>
    <div className="bonds-row-right">
      <div className="bonds-data">
        <div className="bonds-item-part">
          S&P
        </div>
        <div className="bonds-item-part">
          <Form>
            <FormControl className="bonds-text-input" type="text" value={bondPrice.getBondPrice.output['S&P']} onChange={()=>{}} />
          </Form>
        </div>
      </div>
    </div>

    <div className="bonds-row-right">
      <div className="bonds-data">
        <div className="bonds-item-part">
          Moodys
        </div>
        <div className="bonds-item-part">
          <Form>
            <FormControl className="bonds-text-input" type="text" value={bondPrice.getBondPrice.output['Moodys']} onChange={()=>{}}/>
          </Form>
        </div>
      </div>
    </div>

    <div className="bonds-buttons-group-bottom">
      <Button color="blue" theme={theme}>Quote</Button>
      <Button color="orange" theme={theme}>Termsheet</Button>
    </div>

  </div>
)
