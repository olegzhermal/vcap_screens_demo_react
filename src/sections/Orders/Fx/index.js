import React, { Component } from 'react';
import classnames from 'classnames';
import FlipCard from './FlipCard/index.js';
import './index.css';

class Fx extends Component {
  state = {
    activeCard: null,
  }

  activeCheck = (id) => () => {
    if (this.state.activeCard === id) {
      this.setState({ activeCard: null })
    } else {
      this.setState({ activeCard: id })
    }
  }

  render() {
    return (
      <div className="fx-section-container">
        <div className="fx-col">
          <div className={classnames('fx-item-container', `fx-item-container-${this.props.theme}`)}>
            <FlipCard
              cur1="EUR"
              cur2="USD"
              active={this.state.activeCard === 'fc-0'}
              activeCheck={this.activeCheck('fc-0')}
              activeUncheck={this.activeUncheck}
              theme={this.props.theme}
            />
          </div>
          <div className={classnames('fx-item-container', `fx-item-container-${this.props.theme}`)}>
            <FlipCard
              cur1="USD"
              cur2="SPY"
              active={this.state.activeCard === 'fc-1'}
              activeCheck={this.activeCheck('fc-1')}
              activeUncheck={this.activeUncheck}
              theme={this.props.theme}
            />
          </div>
        </div>
        <div className="fx-col">
          <div className={classnames('fx-item-container', `fx-item-container-${this.props.theme}`)}>
            <FlipCard
              cur1="GPB"
              cur2="USD"
              active={this.state.activeCard === 'fc-2'}
              activeCheck={this.activeCheck('fc-2')}
              activeUncheck={this.activeUncheck}
              theme={this.props.theme}
            />
          </div>
          <div className={classnames('fx-item-container', `fx-item-container-${this.props.theme}`)}>
            <FlipCard
              cur1="EUR"
              cur2="GPB"
              active={this.state.activeCard === 'fc-3'}
              activeCheck={this.activeCheck('fc-3')}
              activeUncheck={this.activeUncheck}
              theme={this.props.theme}
            />
          </div>
        </div>
        <div className="fx-col">
          <div className={classnames('fx-item-container', `fx-item-container-${this.props.theme}`)}>
            <FlipCard
              cur1="AUD"
              cur2="USD"
              active={this.state.activeCard === 'fc-4'}
              activeCheck={this.activeCheck('fc-4')}
              activeUncheck={this.activeUncheck}
              theme={this.props.theme}
            />
          </div>
          <div className={classnames('fx-item-container', `fx-item-container-${this.props.theme}`)}>
            <FlipCard
              cur1="AUD"
              cur2="GPB"
              active={this.state.activeCard === 'fc-5'}
              activeCheck={this.activeCheck('fc-5')}
              activeUncheck={this.activeUncheck}
              theme={this.props.theme}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Fx;
