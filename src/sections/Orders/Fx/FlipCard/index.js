import React, {Component} from 'react';
import FrontFlip from './FrontFlip';
import BackFlip from './BackFlip';

export default class FlipCard extends Component {
  state = {
    frontFlip: true,
  }

  flipCard = () => {
    this.setState({frontFlip: !this.state.frontFlip})
  }

  componentWillReceiveProps({ active }) {
    if (!active) this.setState({ frontFlip: true });
  }

  render() {
    const propsToPass = this.props;

    return (
      this.state.frontFlip ?
        <FrontFlip {...propsToPass} onForwardTenorsClick={this.flipCard} /> :
        <BackFlip {...propsToPass} onForwardTenorsClick={this.flipCard} />
    )
  }
}
