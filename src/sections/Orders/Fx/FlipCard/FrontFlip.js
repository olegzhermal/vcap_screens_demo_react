import React from 'react';
import TB3Input from '../../../../common/FormItems/TB3Input.js';
import TGInput from '../../../../common/FormItems/TGInput.js';
import { CalendarIcon } from '../../../../common/icons.js'
import classnames from 'classnames';
import BuySellFields from './BuySellFields';

function getCurrencySign(currency) {
  switch (currency) {
    case 'USD':
    case 'AUD':
      return '$'
    case 'EUR':
      return '€'
    case 'GPB':
      return '£'
    default: return '$';
  }
}

export default ({
  cur1,
  cur2,
  active,
  activeCheck,
  activeUncheck,
  onForwardTenorsClick,
  theme,
}) => (
  <div className={classnames('fx-flipcard', {'fx-flipcard-active': active})}>
    <div className="fx-item-header">
      <span onClick={activeCheck}>{`${cur1} / ${cur2}`}</span>
      <i onClick={onForwardTenorsClick} className={classnames('fa fa-ellipsis-v bodybox-threedot', {'blue-icon': active})} aria-hidden="true" />
    </div>
    <div className="fx-item-body">
      <div>
        <div className="fx-item-body-tb3">
          <TB3Input style={{color: '#1789f5'}} />
        </div>
        <TGInput
          className='fx-item-cur-label'
          onText={`${getCurrencySign(cur1)} ( ${cur1} )`}
          offText={`${getCurrencySign(cur2)} ( ${cur2} )`}
          hwidth={90}
        />
      </div>
      <div className="fx-item-date">
        <span>29/05/2017 (Spot)</span>
        <CalendarIcon
          className={classnames(
            "flip-card-calendar",
            { [`flip-card-calendar-${theme}`]: theme }
          )}
        />
      </div>
      <div className="fx-item-buysell-extra-labels">
        <div>BUY</div>
        <div>SELL</div>
      </div>
      <div className="fx-item-buysell-container">
        <BuySellFields
          active={active}
          arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
          arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
        />
      </div>
      <div className="front-flip-time">Current time</div>
    </div>
    {/*{
      active &&
      <div className="actions-container fx-item-forward-tenor-button-container">
        <Button
          color="green"
          onClick={onForwardTenorsClick}
        >
          Forward Tenors
        </Button>
      </div>
    }*/}
  </div>
)
