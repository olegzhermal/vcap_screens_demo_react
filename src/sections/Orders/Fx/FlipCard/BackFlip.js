import React from 'react';
import classnames from 'classnames';
import Button from '../../../../common/FormItems/Button';
import BuySellFields from './BuySellFields';
import CustomDT from '../../../../common/FormItems/CustomDT.js'

export default ({
  cur1,
  cur2,
  active,
  activeCheck,
  onForwardTenorsClick,
}) => (
  <div className={classnames('fx-flipcard', {'fx-flipcard-active': active})}>
    <div className="fx-item-header-backflip">
      <div>
        <span onClick={activeCheck}>{`${cur1} / ${cur2}`}</span>
        <i onClick={onForwardTenorsClick} className={classnames('fa fa-ellipsis-v bodybox-threedot', {'blue-icon': active})} aria-hidden="true" />
      </div>
      <div className="fx-item-buysell-extra-labels-backflip">
        <div>BUY</div>
        <div>SELL</div>
      </div>
    </div>
    <div className="fx-item-body fx-item-backflip">
      <div>
        {/*(div[className="backflip-buysell-item-container"]>lormem2)*20*/}
        <div className="backflip-buysell-item-container">
          <BuySellFields
            {...{active, extraInfo: true}}
            arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
            arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
          />
        </div>
        <div className="backflip-buysell-item-container">
          <BuySellFields
            {...{active, extraInfo: true}}
            arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
            arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
          />
        </div>
        <div className="backflip-buysell-item-container">
          <BuySellFields
            {...{active, extraInfo: true}}
            arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
            arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
          />
        </div>
        <div className="backflip-buysell-item-container">
          <BuySellFields
            {...{active, extraInfo: true}}
            arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
            arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
          />
        </div>
        <div className="backflip-buysell-item-container">
          <BuySellFields
            {...{active, extraInfo: true}}
            arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
            arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
          />
        </div>
        <div className="backflip-buysell-item-container">
          <BuySellFields
            {...{active, extraInfo: true}}
            arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
            arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
          />
        </div>
        <div className="backflip-buysell-item-container">
          <BuySellFields
            {...{active, extraInfo: true}}
            arrowDirectionLeftside={Math.random() >= 0.5 ? 'up' : 'down'}
            arrowDirectionRightside={Math.random() >= 0.5 ? 'up' : 'down'}
          />
        </div>
      </div>
      <div className="backflip-current-date">
        <span onClick={activeCheck}>30/05/2017</span>
      </div>
      <div className="backflip-bottom-actions-container">
        <CustomDT className="backflip-calendar"/>
        <Button
          color="green"
          onClick={onForwardTenorsClick}
        >
          Price
        </Button>
      </div>
    </div>
  </div>
)
