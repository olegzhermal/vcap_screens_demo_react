import React, {Component} from 'react';
import classnames from 'classnames';
import Arrow from '../../../../common/Arrow.js';
import './buySellFields.css';

export default class BuySellFields extends Component {
  state = {
    active: null,
  }

  changeActive = (side) => () => {
    if (side === this.state.active) {
      this.setState({active: null});
    } else {
      this.setState({active: side})
    }
  }

  render() {
    const {
      active,
      extraInfo,
      arrowDirectionLeftside,
      arrowDirectionRightside,
    } = this.props;

    return (
      <div className="buy-sell-fields-main">
        <Arrow direction={arrowDirectionLeftside} className='buy-sell-arrow buy-sell-arrow-left'/>
        <Arrow direction={arrowDirectionRightside} className='buy-sell-arrow buy-sell-arrow-right'/>
        <div
          onClick={this.changeActive('left')}
          className={classnames(
            'buy-sell-fields-tab-left',
            {'buy-sell-fields-tab-active': active === 'left' && active},
            {'buy-sell-item-active': active}
          )}
        >
          <div className="buy-sell-fields-item-left">
            1.11
          </div>
          <div className="buy-sell-fields-item-right">
            <div>
              49
            </div>
            <div>
              9
            </div>
          </div>
          <div>
            {
              extraInfo &&
              <div className="buy-sell-fields-tab-middle-top">
                W1
              </div>
            }
            <div className="buy-sell-fields-tab-middle-middle">
              13.3
            </div>
          </div>
        </div>
        <div
          onClick={this.changeActive('right')}
          className={classnames(
            'buy-sell-fields-tab-right',
            {'buy-sell-fields-tab-active': this.state.active === 'right' && active},
            {'buy-sell-item-active':active}
          )}
        >
          <div className="buy-sell-fields-item-left">
            1.11
          </div>
          <div className="buy-sell-fields-item-right">
            <div>
              50
            </div>
            <div>
              1
            </div>
          </div>
        </div>
      </div>
    )
  }
}
