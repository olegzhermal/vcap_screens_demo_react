import React from 'react';
import './index.css';
import { Row, Col } from 'react-bootstrap';

const FxOptionSectionItem = ({label, input, note}) => {
  return (
    <Row className="fix-option-table-item">
      <Col className="fix-option-label" md={4}>{label}</Col>
      <Col md={4}>{input}</Col>
      <Col md={4} className="fix-option-note">{note}</Col>
    </Row>
  );
}

export default FxOptionSectionItem;
