import React from 'react';
import classnames from 'classnames';
import OrdersHeader from './OrdersHeader';
import './index.css';
import getOrdersBody from './routerImitation';

export default ({ bodySection, theme, ordersBodySection, changeOrdersBody, }) => {
  const OrdersBody = getOrdersBody(bodySection);
  const styles = {
    ordersContainer: classnames(
      'orders-container',
      `orders-container-${theme}`,
    ),
  }

  return (
    <div className={styles.ordersContainer}>
      <OrdersHeader {...{changeOrdersBody, ordersBodySection}} />
      <OrdersBody theme={theme}/>
    </div>
  );
}
