import React from 'react';
import './index.css';
import { Row, Col } from 'react-bootstrap';

const GridDoubleCell = ({label, input, note}) => {
  return (
    <Row className="grid-tripple-cell">
      <Col className="grid-tripple-cell-label" md={4}>{label}</Col>
      <Col md={8}>{input}</Col>
    </Row>
  );
}

export default GridDoubleCell;
