import React from 'react';
import './index.css';
import { Row, Col } from 'react-bootstrap';

const GridTrippleCell = ({label, input, note, styles}) => {
  const labelStyle = styles && styles.label ? `grid-tripple-cell-label ${styles.label}` : 'grid-tripple-cell-label';
  const inputStyle = styles && styles.input ? `grid-tripple-cell-input ${styles.input}` : 'grid-tripple-cell-input';
  const noteStyle = styles && styles.note ? `grid-tripple-cell-note ${styles.note}` : 'grid-tripple-cell-note';

  return (
    <Row className="grid-tripple-cell">
      <Col className={labelStyle} md={4}>{label}</Col>
      <Col className={inputStyle} md={4}>{input}</Col>
      <Col className={noteStyle} md={4}>{note}</Col>
    </Row>
  );
}


export default GridTrippleCell;
