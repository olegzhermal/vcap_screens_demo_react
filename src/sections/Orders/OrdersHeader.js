import React from 'react';
import classnames from 'classnames';

const ordersMenuData = [
  {
    text: 'FX',
    key: 'fx',
  },
  {
    text: 'FX OPTION',
    key: 'fx option',
  },
  {
    text: 'BONDS',
    key: 'bonds',
  },
  {
    text: 'LOAN/DEPO',
    key: 'loan/depo',
  },
  {
    text: 'STR.DEPO',
    key: null,
  },
  {
    text: 'DCD',
    key: 'dcd',
  },
];

export default ({ changeOrdersBody, ordersBodySection }) => {
  return (
    <nav className="orders-nav">
      {ordersMenuData.map( ({ text, key }) => (
        <div
          key={key}
          onClick={ key ? changeOrdersBody(key) : null }
          className={ classnames({'orders-header-item-highlighted': key === ordersBodySection}) }
        >
          {text}
        </div>
      ))}
    </nav>
  )
}
