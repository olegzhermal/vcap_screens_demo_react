import React from 'react';
import Button from '../../../common/FormItems/Button';
import TB2Input from '../../../common/FormItems/TB2Input.js';
import TB3Input from '../../../common/FormItems/TB3Input.js';
import TB4Input from '../../../common/FormItems/TB4Input.js';
import RSInput from '../../../common/FormItems/RSInput.js';
import CustomDT from '../../../common/FormItems/CustomDT.js';
import TGInput from '../../../common/FormItems/TGInput.js';
import priceFXOption from '../../../fixtures/SampleJSON/priceFXOptionResp.js';
import onFXOptionLoadCtp from '../../../fixtures/SampleJSON/onFXOptionLoad-ctp.js';
import onFXOptionLoad from '../../../fixtures/SampleJSON/onFXOptionLoad-inst.js';
import './index.css';

const rsInputOptions = onFXOptionLoadCtp.counterpart.map(({ label }) => (
  {
    value: label,
    label
  }
))

const instrumentOptions = onFXOptionLoad.instrument.map(value => (
  {
    value,
    label: value
  }
));

export default ({theme}) => (
  <div className={`orders-section-container fx-option-container-${theme}`}>
    {/*(div[className="fx-option-row"]>(div[className="fx-option-data"]>(div[className="fx-option-item-part"]>lorem2)*3)*3)*3*/}
    {/*(div[className="fx-option-row"]>(div[className="fx-option-data"]>(div[className="fx-option-item-part"]>lorem2)*3)*2)*3*/}
    <div className="fx-option-row">
      <div className="fx-option-data fx-option-col1">
        <div className="fx-option-item-part">
          Counterparty
        </div>
        <div className="fx-option-item-part">
          <RSInput value="" options={rsInputOptions}/>
        </div>
        <div className="fx-option-item-part color-gold">
          Gold
        </div>
      </div>
      <div className="fx-option-data fx-option-col2">
        <div className="fx-option-item-part">
          Expiry
        </div>
        <div className="fx-option-item-part">
          <CustomDT {...{theme}}/>
        </div>
        <div className="fx-option-item-part fx-option-small-note">
          (till: <span>12.04.17'</span>)
        </div>
      </div>
      <div className="fx-option-data">
        <div className="fx-option-item-part">
          Margin
        </div>
        <div className="fx-option-item-part">
          <TB2Input className="fx-option-text-input margin-input fx-option-inputs" />
        </div>
        <div className="fx-option-item-part">
          <TGInput
            className="fx-option-text-input margin-input fx-option-inputs"
            onText="EUR"
            offText="USD"
          />
        </div>
      </div>
    </div>
    <div className="fx-option-row">
      <div className="fx-option-data fx-option-col1">
        <div className="fx-option-item-part">
          Instrument
        </div>
        <div className="fx-option-item-part">
          <RSInput value="" options={instrumentOptions}/>
          {/*<DropdownButton id="dd-0" style={{color: '#A9B1B6'}} className={`fx-option-inputs dropdown-button fx-option-dropdown-${theme}`} bsStyle="default" title="EUR/USD">
            {
              onFXOptionLoad.instrument.map((instrument, index) => (
                <MenuItem eventKey={index} key={index}>{instrument}</MenuItem>
              ))
            }
          </DropdownButton>*/}
        </div>
      </div>
      <div className="fx-option-data fx-option-col2">
        <div className="fx-option-item-part">
          Strike
        </div>
        <div className="fx-option-item-part">
          <TB4Input className="fx-option-text-input margin-input fx-option-inputs" value= {priceFXOption.fxoption_pricing_resp.output["strike"]} />
        </div>
      </div>
      <div className="fx-option-data">
        <div className="fx-option-item-part">
          Amount
        </div>
        <div className="fx-option-item-part">
          <TB3Input className="fx-option-text-input margin-input fx-option-inputs" />
        </div>
        <div className="fx-option-item-part">
          EUR
        </div>
      </div>
    </div>
    <div className="fx-option-row">
      <div className="fx-option-data fx-option-col1">
        <div className="fx-option-item-part">
          Call/Put
        </div>
        <div className="fx-option-item-part">
          <TGInput
            className="fx-option-text-input margin-input fx-option-inputs"
            onText="Call EUR / Put USD"
            offText="Put EUR / Call USD"
            hwidth={153}
          />
        </div>
      </div>
      <div className="fx-option-data fx-option-col2">
        <div className="fx-option-item-part">
          Nominal
        </div>
        <div className="fx-option-item-part">
          <TB3Input className="fx-option-text-input margin-input fx-option-inputs" />
        </div>
        <div className="fx-option-item-part">
          <TGInput
            className="fx-option-text-input margin-input fx-option-inputs"
            onText="Buy"
            offText="Sell"
          />
        </div>
      </div>
      <div className="fx-option-data fx-option-action-buttons-container">
        <Button color="blue" theme={theme}>Price</Button>
      </div>
    </div>

    <hr/>

    <div className="fx-option-row">
      <div className="fx-option-data fx-option-col1">
        <div className="fx-option-item-part">
          Premium
        </div>
        <div className="fx-option-item-part">
          <TB3Input className="fx-option-text-input margin-input fx-option-inputs" value= {priceFXOption.fxoption_pricing_resp.output["premium"]} />
        </div>
        <div className="fx-option-item-part">
          EUR
        </div>
      </div>
      <div className="fx-option-data fx-option-col2">
        <div className="fx-option-item-part">
          Strike
        </div>
        <div className="fx-option-item-part">
          <TB4Input className="fx-option-text-input margin-input fx-option-inputs" value= {priceFXOption.fxoption_pricing_resp.output["strike"]} />
        </div>
      </div>
      <div className="fx-option-data">
        <div className="fx-option-item-part">
          Spot
        </div>
        <div className="fx-option-item-part">
          <TB4Input className="fx-option-text-input margin-input fx-option-inputs" value= {priceFXOption.fxoption_pricing_resp.output["spot"]} />
        </div>
      </div>
    </div>
    <div className="fx-option-row">
      <div className="fx-option-data fx-option-col1">
        <div className="fx-option-item-part">
          Current Vol.
        </div>
        <div className="fx-option-item-part">
          <TB2Input className="fx-option-text-input margin-input fx-option-inputs" />
        </div>
      </div>
      <div className="fx-option-data fx-option-col2">
        <div className="fx-option-item-part">
          Delta
        </div>
        <div className="fx-option-item-part">
          <TB2Input className="fx-option-text-input margin-input fx-option-inputs" value= {priceFXOption.fxoption_pricing_resp.output["delta"]} />
        </div>
      </div>
      <div className="fx-option-data fx-option-action-buttons-container">
        <Button color="blue" theme={theme}>Quote</Button>
        <Button color="orange" theme={theme}>Termsheet</Button>
      </div>
    </div>

  </div>
);
