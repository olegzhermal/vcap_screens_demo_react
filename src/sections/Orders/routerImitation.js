import Fx from './Fx/index.js';
import FxOption from './FxOption/index.js';
import Bonds from './Bonds/index.js';
import LoanDepo from './LoanDepo/index.js';
import Dcd from './Dcd/index.js';

export default (name) => {
  switch (name) {
    case 'fx':
      return Fx;
    case 'fx option':
      return FxOption;
    case 'bonds':
      return Bonds;
    case 'loan/depo':
      return LoanDepo;
    case 'dcd':
      return Dcd;
    default:

  }
}
