import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import CustomDT from '../../../common/FormItems/CustomDT.js';
import TB2Input from '../../../common/FormItems/TB2Input.js';
import TB3Input from '../../../common/FormItems/TB3Input.js';
import RSInput from '../../../common/FormItems/RSInput.js';
import Button from '../../../common/FormItems/Button';
import TGInput from '../../../common/FormItems/TGInput.js';
import onLoanDepoLoadCtp from '../../../fixtures/SampleJSON/onLoanDepoLoad-ctp.js';
import onLoanDepoLoad from '../../../fixtures/SampleJSON/onLoanDepoLoad-inst.js';
import priceLoanDepo from '../../../fixtures/SampleJSON/priceLoanDepoResp.js';
import './index.css';

const rsInputOptions = onLoanDepoLoadCtp.counterpart.map( ({ label }) => (
  {
    value: label,
    label
  }
))

const instrumentOptions = onLoanDepoLoad.fixed.instrument.map(value => (
  {
    value,
    label: value
  }
));

export default ({theme}) => (
  <div className={`orders-section-container loan-depo-container-${theme}`}>
    {/*(div[className="loan-depo-row"]>(div[className="loan-depo-data"]>(div[className="loan-depo-item-part"]>lorem2)*3)*3)*3*/}
    {/*(div[className="loan-depo-row"]>(div[className="loan-depo-data"]>(div[className="loan-depo-item-part"]>lorem2)*3)*2)*3*/}
    <div className="loan-depo-row">
      <div className="loan-depo-data loan-depo-col1">
        <div className="loan-depo-item-part">
          Counterparty
        </div>
        <div className="loan-depo-item-part">
          <RSInput value="" options={rsInputOptions}/>
        </div>
        <div className="loan-depo-item-part color-gold">
          Gold
        </div>
      </div>
      <div className="loan-depo-data loan-depo-col2">
        <div className="loan-depo-item-part">
          Nature
        </div>
        <div className="loan-depo-item-part">
          <TGInput
            className="loan-depo-dropdown loan-depo-inputs dropdown-button"
            onText="Fixed"
            offText="Floating"
            hwidth={90}
          />
        </div>
      </div>
      <div className="loan-depo-data">
        <div className="loan-depo-item-part">
          Rate
        </div>
        <div className="loan-depo-item-part">
          <TB2Input value={priceLoanDepo.loandepo_pricing_resp.output['rate']}/>
        </div>
      </div>
    </div>
    <div className="loan-depo-row">
      <div className="loan-depo-data loan-depo-col1">
        <div className="loan-depo-item-part">
          Instrument
        </div>
        <div className="loan-depo-item-part">
          <RSInput value="" options={instrumentOptions}/>
        </div>
      </div>
      <div className="loan-depo-data loan-depo-col2">
        <div className="loan-depo-item-part">
          Nominal
        </div>
        <div className="loan-depo-item-part">
          <TB3Input style={{color: '#1789F5'}} />
        </div>
        <div className="loan-depo-item-part">
          <TGInput
            className="loan-depo-dropdown loan-depo-inputs dropdown-button"
            onText="EUR"
            offText="USD"
            hwidth={90}
          />
        </div>
      </div>
    </div>
    <div className="loan-depo-row">
      <div className="loan-depo-data loan-depo-col1">
        <div className="loan-depo-item-part">
          Start
        </div>
        <div className="loan-depo-item-part">
          <CustomDT theme={theme}/>
        </div>
      </div>
      <div className="loan-depo-data loan-depo-col2">
        <div className="loan-depo-item-part">
          Expiry
        </div>
        <div className="loan-depo-item-part">
          <CustomDT theme={theme}/>
        </div>
      </div>
    </div>
    <div className="loan-depo-row">
      <div className="loan-depo-data loan-depo-col1">
        <div className="loan-depo-item-part">
          Amortizing
        </div>
        <div className="loan-depo-item-part">
          <DropdownButton id="dd-5" className={`loan-depo-dropdown-${theme} loan-depo-inputs dropdown-button`} bsStyle="default" title="C/P">
            <MenuItem eventKey="1">Option 1</MenuItem>
            <MenuItem eventKey="2">Option 2</MenuItem>
            <MenuItem eventKey="3">Option 3</MenuItem>
          </DropdownButton>
        </div>
      </div>
      <div className="loan-depo-data loan-depo-price-button-container">
        <Button color="blue" theme={theme}>Price</Button>
      </div>
    </div>

    <hr/>

    <div className="loan-depo-row">
      <div className="loan-depo-data loan-depo-col1">
        <div className="loan-depo-item-part">
          Rate
        </div>
        <div className="loan-depo-item-part">
          <TB2Input style={{color: '#1789F5'}} value={priceLoanDepo.loandepo_pricing_resp.output['rate']} />
        </div>
      </div>
      <div className="loan-depo-data loan-depo-col2">
        <div className="loan-depo-item-part">
          Net Rate
        </div>
        <div className="loan-depo-item-part">
          <TB2Input style={{color: '#1789F5'}} value={priceLoanDepo.loandepo_pricing_resp.output['net rate']} />
        </div>
        <div className="loan-depo-item-part">
          EUR
        </div>
      </div>
      <div className="loan-depo-data">
        <div className="loan-depo-item-part">
          Repayment
        </div>
        <div className="loan-depo-item-part">
          <TB3Input style={{color: '#1789F5'}} value={priceLoanDepo.loandepo_pricing_resp.output['repayment']} />
        </div>
      </div>
    </div>
    <div className="loan-depo-row">
      <div className="loan-depo-data loan-depo-col1">
        <div className="loan-depo-item-part">
          Margin
        </div>
        <div className="loan-depo-item-part">
          <TB2Input style={{color: '#1789F5'}} value={priceLoanDepo.loandepo_pricing_resp.output['margin']} />
        </div>
      </div>
      <div className="loan-depo-data loan-depo-col2">
        <div className="loan-depo-item-part">
          Margin Amount
        </div>
        <div className="loan-depo-item-part">
          <TB3Input style={{color: '#1789F5'}} />
        </div>
      </div>
    </div>
    <div className="loan-depo-row loan-depo-buttons-group-bottom">
      <Button color="grey" theme={theme}>Schedule</Button>
      <Button color="blue" theme={theme}>Quote</Button>
      <Button color="orange" theme={theme}>Termsheet</Button>
    </div>
  </div>
);
