import React from 'react';

export default function AccountCard(props) {

    return (
      <div className="account-card">
        <div className="account-card-top">
            <div className="account-card-top-icon"></div>
            <div className="account-card-top-body">
              <p><span>{"Name of the company:"}</span><span className="account-card-data">{"User"}</span></p>
              <p><span>{"Industry:"}</span><span className="account-card-data">{"User"}</span></p>
              <p><span>{"Rating:"}</span><span className="account-card-data-rating">{"User"}</span></p>
              <p><span>{"Tier:"}</span><span className="account-card-data-tier">{"User"}</span></p>
              <p><span>{"PnL Summary:"}</span><span className="account-card-data">{"User"}</span></p>
            </div>
        </div>
        <div className="account-card-bottom">
          <div>
              <p className="contact-icon">
              <i className="fa fa-user" aria-hidden="true"></i>
              </p>
              <p className="contact-icon">
              <i className="fa fa-envelope" aria-hidden="true"></i>
              </p>
              <p className="contact-icon">
              <i className="fa fa-mobile" aria-hidden="true"></i>
              </p>
          </div>
          <div>
              <p><span className="contact">{"Contact person:"}</span><span className="account-card-data">{"User"}</span></p>
              <p><span className="contact">{"Contact email:"}</span><span className="account-card-data">{"User"}</span></p>
              <p><span className="contact">{"Contact telephone:"}</span><span className="account-card-data">{"User"}</span></p>
          </div>
        </div>
      </div>
    )
  }
