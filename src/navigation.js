import {
  DashboardIcon,
  AccountsIcon,
  OrdersIcon,
  CampaignsIcon,
  AnalyticsIcon,
  ReportsIcon,
  EmailIcon,
  SettingsIcon,
} from './common/icons.js'

export default [
  {
    text: 'Dashboard',
    link: '',
    Icon: DashboardIcon,
    viewName: 'dashboard',
  },
  {
    text: 'Accounts',
    link: '',
    Icon: AccountsIcon,
    viewName: 'accounts',
  },
  {
    text: 'Orders',
    link: '',
    Icon: OrdersIcon,
    viewName: 'orders',
  },
  {
    text: 'Campaigns',
    link: '',
    Icon: CampaignsIcon,
  },
  {
    text: 'Analytics',
    link: '',
    Icon: AnalyticsIcon,
  },
  {
    text: 'Reports',
    link: '',
    Icon: ReportsIcon,
  },
  {
    text: 'Email',
    link: '',
    Icon: EmailIcon,
  },
  {
    text: 'Settings',
    link: '',
    Icon: SettingsIcon,
  },
]
