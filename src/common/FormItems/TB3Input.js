import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { FormControl } from 'react-bootstrap';

export default class TB3Input extends Component {
  state = {
    value: this.props.value,
  }

  _onChange = (e, value) => {
    this.setState({ value });
  }
  _onKeyPress =( {key} ) => {
    console.log(key);
    if (key === 'k' || key === 'K') {
      this.setState({ value: this.state.value * 1000 })
    } else if (key === 'm' || key === 'M') {
      this.setState({ value: this.state.value * 1000000 })
    }
  }

  render() {
    const { value } = this.state
    return (
      <NumberFormat
        customInput={FormControl}
        thousandSeparator={true}
        value={value}
        style={this.props.style}
        onChange={this._onChange}
        onKeyPress={this._onKeyPress}
      />
    )
  }
}
