import React from 'react';
import { Button } from 'react-bootstrap';
import classnames from 'classnames';
import './index.css';

export default ({ children, color, theme, onClick }) => {
  return (
    <Button
      className={classnames(
        'st-char-button',
        {[`st-char-button-${color}`]: ['blue', 'grey', 'orange', 'green'].includes(color)},
        {[`st-char-button-${theme}`]: theme},
      )}
      onClick={onClick}
    >
      {children}
    </Button>
  )
}
