import React from 'react';
import './inputStyles.css';
import "react-day-picker/lib/style.css";
import Switch from 'react-bootstrap-switch';
import 'react-bootstrap-switch/dist/css/bootstrap3/react-bootstrap-switch.css';
import './inputStyles.css';

export default ({ onText, offText, lwidth, hwidth, extraStyle }) => (
    <Switch
      name='test'
      onText={onText}
      offText={offText}
      bsSize='noraml'
      wrapperClass="toggle-input"
      baseClass='toggle-input bootstrap-switch'
      onColor="success"
      offColor="warning"
      handleWidth={hwidth ? hwidth : 60}
      labelWidth={lwidth ? lwidth : 10}
    />
)
