import React, { Component } from 'react';
import { Form, FormControl } from 'react-bootstrap';

// const decimalOrEmptyStringOnly = /^$|^(\d+\.?\d{0,9}|\.\d{1,9})$/;
const decimalOrEmptyStringOnly = /^$|^(\d+\.?\|\,?\d{0,9}|\.\d{1,9})$/;

function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default class TB2Input extends Component {
  state = {
    value: "",
  }

  onChange = ({ target: {value} }) => {
    if (!decimalOrEmptyStringOnly.test(value)) return;
    if (value.split('.')[1] && value.split('.')[1].length > 2) return;
    this.setState({ value });
  }

  render() {
    return (
      <Form>
        <FormControl
          className={this.props.className}
          type="text"
          defaultValue={(Math.random()*10).toFixed(4)}
          value={this.state.value}
          onChange={this.onChange}
        />
      </Form>
    )
  }
}
