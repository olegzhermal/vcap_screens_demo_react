import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import './inputStyles.css';

export default function ({ value, options, onChange }) {
  return (
    <Select className="rs-input" {...{ value, options, onChange }} placeholder=""/>
  )
}
