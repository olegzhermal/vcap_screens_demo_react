import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { FormControl } from 'react-bootstrap';
import './inputStyles.css';

export default class TB2Input extends Component {
  state = {
    value: this.props.value,
  }

  _onChange = (e, value) => {
    this.setState({ value });
  }

  render() {
    const { value } = this.state
    return (
      <NumberFormat
        customInput={FormControl}
        className='text-input'
        thousandSeparator={true}
        onChange={this._onChange}
        style={this.props.style}
        value={value}
      />
    )
  }
}
