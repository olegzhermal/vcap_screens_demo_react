import React, { Component } from 'react';
import { FormControl } from 'react-bootstrap';
import moment from 'moment';
import classnames from 'classnames';
import {CalendarIcon} from '../icons';
import './inputStyles.css';
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css"

const sixDigitRegex = /^\d{6,6}$/;
const eightDigitRegex = /^\d{8,8}$/;

export default class CustomDT extends Component {
  state = {
    value: '',
    dayPickerShown: false,
  }

  _onChange = (e) => {
    this.setState({ value: e.target.value });
  }

  _checkInput = () => {
    const {value} = this.state;
    if (sixDigitRegex.test(value)) {
      if (moment(value, 'DDMMYY').isValid()) {
        const formatedDate = moment(value, 'DDMMYY').format('DD/MM/YYYY');
        this.setState({value: formatedDate});
      } else {
        this.setState({value: ''});
      }
    } else if (eightDigitRegex.test(value)) {
      if (moment(value, 'DDMMYYYY').isValid()) {
        const formatedDate = moment(value, 'DDMMYYYY').format('DD/MM/YYYY');
        this.setState({value: formatedDate});
      } else {
        this.setState({value: ''});
      }
    } else if (['spot', 'SPOT', 'tom', 'TOM'].includes(value)) {
      return;
    } else {
      const lastChar = value.slice(-1);
      if (['d', 'D', 'w', 'W', 'm', 'M', 'y', 'Y'].includes(lastChar)) {
        if (!/^\d{1,3}$/.test(value.slice(0,-1))) {
          this.setState({value:''})
        }
      } else {
        this.setState({value:''})
      }
    }
  }

  toggleDayPicker = () => {
    this.setState({dayPickerShown: !this.state.dayPickerShown})
  }

  handleDayClick = (day, { selected }) => {
    const dateToSet = moment(day).format('DD/MM/YYYY');
    this.setState({value: dateToSet});
    this.toggleDayPicker();
  };

  render() {
    return (
      <div className={classnames("dt-input", {[this.props.className]: this.props.className})}>
        <CalendarIcon
          onClick={this.toggleDayPicker}
          className={classnames("dt-calendar-icon", {[`dt-calendar-icon-${this.props.theme}`]:this.props.theme})}
        />
        <FormControl
          value={ this.state.value}
          onChange={this._onChange}
          onBlur={this._checkInput}
        />
        {
          this.state.dayPickerShown && (
          <div className="day-picker-container">
            <DayPicker
              disabledDays={{ daysOfWeek: [0] }}
              selectedDays={this.state.selectedDay}
              onDayClick={this.handleDayClick}
              className='hmmm'
            />
          </div>
        )
        }
      </div>

    )
  }
}
