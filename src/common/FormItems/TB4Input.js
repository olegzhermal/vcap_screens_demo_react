import React, { Component } from 'react';
import { FormControl } from 'react-bootstrap';

const decimalOrEmptyStringOnly = /^$|^(\d+\.?\d{0,9}|\.\d{1,9})$/;
const twoDigitRegex = /^-?\d{1,2}$/;

export default class TB4Input extends Component {
  state = {
    value: this.props.value,
  }

  _onChange = (e) => {
    this.setState({ value: e.target.value });
  }

  _checkInput = () => {
    const {value} = this.state;
    if (value === 'ATM' || value === 'atm') {
      return;
    } else if (value.indexOf('DS') !== -1) {
      const secondCheck = value.split('DS');
      if ( secondCheck.length !== 2 || secondCheck[1] !== "" || !twoDigitRegex.test(secondCheck[0]) ) {
        this.setState({ value: '' });
      }
    } else if (value.indexOf('ds') !== -1) {
      const secondCheck = value.split('ds');
      if ( secondCheck.length !== 2 || secondCheck[1] !== "" || !twoDigitRegex.test(secondCheck[0]) ) {
        this.setState({ value: '' });
      }
    } else if (value.indexOf('d') !== -1) {
      const secondCheck = value.split('d');
      if (secondCheck.length !== 2 || secondCheck[1] !== "" || !twoDigitRegex.test(secondCheck[0])) {
        this.setState({ value: '' });
      }
    } else if (value.indexOf('D') !== -1) {
      const secondCheck = value.split('D');
      if (secondCheck.length !== 2 || secondCheck[1] !== "" || !twoDigitRegex.test(secondCheck[0])) {
        this.setState({ value: '' });
      }
    } else if (!decimalOrEmptyStringOnly.test(value)) {
      this.setState({ value: '' });
    }
  }

  render() {
    return (
      <FormControl
        value={ this.state.value}
        onChange={this._onChange}
        onBlur={this._checkInput}
      />
    )
  }
}
