import React, { Component } from 'react';
import DatePicker from 'react-bootstrap-date-picker';
import './inputStyles.css';

export default class TB2Input extends Component {
  state = {
    value: new Date().toISOString(),
  }

  _onChange = (value) => {
    this.setState({ value });
  }

  render() {
    const { value } = this.state;

    return (
      <DatePicker
        value={value}
        onChange={this._onChange}
      />
    )
  }
}
