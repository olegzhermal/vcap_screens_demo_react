import React from 'react';
import './index.css';

export default ({header, children}) => (
  <div className="bodybox-container">
    <div className="bodybox-head">
      <div>
        {header}
      </div>
      <i className="fa fa-ellipsis-v bodybox-threedot" aria-hidden="true" />
    </div>
    <div className="bodybox-body">
      {children}
    </div>
  </div>
)
