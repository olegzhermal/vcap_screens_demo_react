import React from 'react';
import classnames from 'classnames';
import './icons.css';

const iconsPath = '../assets/ui_kit/icons/'

const style = {
  bars: {
    backgroundImage: `url(${iconsPath}/bars.svg)`,
  },
}

export function Bars() {
  return <div style={style.bars} />
}

export function CalendarIcon({className, onClick}) {
  const iconClassName = classnames('calendar-icon',  {[className]: className});
  return (
    <div className={iconClassName} onClick={onClick} />
  )
}
export function DashboardIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'dashboard-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
export function AccountsIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'accounts-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
export function OrdersIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'orders-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
export function CampaignsIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'campaigns-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
export function AnalyticsIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'analytics-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
export function ReportsIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'reports-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
export function EmailIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'email-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
export function SettingsIcon({theme}) {
  return (
    <div className={classnames('aside-navigation-icon', 'settings-icon', {[`icon-${theme}`]: ['light', 'dark'].includes(theme)})} />
  )
}
