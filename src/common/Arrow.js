import React from 'react';
import classnames from 'classnames';
import './Arrow.css';

export default ({ direction, className }) => (
  <div className={classnames(
    'updown-arrows',
    {[`arrow-${direction}`]: ['up', 'down'].includes(direction)},
    {[className]: className},
    )}
  />
)
