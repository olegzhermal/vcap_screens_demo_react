import React from 'react';
import './index.css';

export default ({children, onClick}) => (
  <div className="notificationbox-container" onClick={onClick}>
    {children}
  </div>
)
