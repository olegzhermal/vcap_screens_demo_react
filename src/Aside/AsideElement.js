import React from 'react';
import './AsideElement.css';
import classnames from 'classnames';

const AsideElement = ({
  children,
  link,
  Icon,
  theme,
  onClick,
  highlighted,
  navigationHidden,
}) => {
  const className = classnames(
    'aside-element',
    `aside-element-${theme}`,
    {[`aside-element-highlighted-${theme}`]: highlighted},
  );
  return (
    <div {...{className, onClick}} >
      <div className={classnames('icon-container', {'icon-to-the-right': navigationHidden})}>
        <Icon {...{theme}} />
      </div>
      <a>{children}</a>
    </div>
  )
};

export default AsideElement;
