import React from 'react';
import AsideElement from './AsideElement';
import './index.css';
import classNames from 'classnames';

const Aside = ({
  data,
  theme,
  section,
  changeView,
  toggleNavigation,
  navigationHidden,
}) => {
  const asideClassnames = {
    container: classNames(
      'aside-container',
      `aside-container-${theme}`,
      {'aside-container-hidden': navigationHidden},
    ),
    icons: classNames('fa icon'),
  };

  return (
    <aside className={asideClassnames.container}>
      <div>
        <div className="aside-head">
          <div className="logo-image" />
          <i
            className={`fa-bars ${asideClassnames.icons}`}
            aria-hidden="true"
            onClick={toggleNavigation}
          />
        </div>
      </div>
      <nav className="aside-navigation">
        {
          data.map( ({text, link, Icon, viewName}, index) => {
            const key = `aside-${index}`;
            const highlighted = section === viewName;
            const onClick = viewName ? changeView(viewName) : null;

            return (
            <AsideElement {...{ key, Icon, theme, onClick, highlighted, navigationHidden }}>
              {text}
            </AsideElement>
            )
          })
        }
      </nav>
    </aside>
  )
}

export default Aside;
