import React, { Component } from 'react';
import './App.css';
import Dashboard from './sections/Dashboard';
import Accounts from './sections/Accounts';
import Orders from './sections/Orders';
import Aside from './Aside';
import asideMenuData from './navigation';
import Header from './Header';
import classnames from 'classnames';

class App extends Component {
  state = {
    theme: 'light',
    section: 'orders',
    ordersBodySection: 'fx option',
    navigationHidden: false,
  }

  changeView = (name) => () => {
    this.setState({section: name});
  }

  changeTheme = (name) => () => {
    this.setState({theme: name});
  }

  toggleNavigation = () => {
    this.setState({navigationHidden: !this.state.navigationHidden});
  }

  changeOrdersBody = (ordersBodySection) => () => {
    this.setState({ ordersBodySection });
  }

  render() {
    const {
      changeTheme,
      changeView,
      toggleNavigation,
      state,
      changeOrdersBody,
    } = this;

    const {
      theme,
      navigationHidden,
      section,
      ordersBodySection,
    } = state;

    const Body = getBody(this.state.section);
    const bodySection = ordersBodySection;

    const styles = {
      mainContainer: classnames(
        'main-container',
        {'main-container-navigation-hidden': navigationHidden},
      ),
    }

    return (
      <div className={`app-container theme-${theme}`}>
        <Aside
          data={asideMenuData}
          {...{ theme, section, changeView, toggleNavigation, navigationHidden }}
        />
        <div className={styles.mainContainer}>
          <Header {...{
            changeTheme,
            navigationHidden,
            section,
            theme,
          }} />
          <div className={`body-container body-container-${theme}`}>
            <Body {...{ bodySection, theme, ordersBodySection, changeOrdersBody, }} />
          </div>
        </div>
      </div>
    );
  }
}

function getBody(viewName) {
  switch (viewName) {
    case 'dashboard':
      return Dashboard;
    case 'accounts':
      return Accounts;
    case 'orders':
      return Orders;
    default: return Dashboard;
  }
}

export default App;
